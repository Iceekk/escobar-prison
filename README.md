# EscobarPrison

Prison Escobar description

**Team Name: Team Escobar**

List of members:
  - Georgi Levov [telerikacademy.com/Users/GeorgeLevov](https://my.telerikacademy.com/Users/GeorgeLevov)
  - Hristo Mirchev [telerikacademy.com/Users/iceekk](https://my.telerikacademy.com/Users/iceekk)
 
### Project Purpose

> Secure software solution for prisons

### URL of Repository 
https://gitlab.com/Iceekk/escobar-prison

### Features

  - Admin/User/Guest roles, jwt authentication, angular login form
  - Admin services for manipulating user,event,cell,account data in the database 
  - Users with many datapoints: accounts, items, cell, events
