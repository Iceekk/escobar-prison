import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../core/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from '../../core/alert.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  isLoggedIn: Observable<boolean>;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthService,
    private alertService: AlertService,

  ) {
    this.isLoggedIn = authenticationService.isLoggedIn();

  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    const formData = this.loginForm.value;
    console.log(formData.username);
    this.authenticationService.login(formData.username, formData.password)
      .subscribe(
        (data: any) => {
          localStorage.setItem('token', data.json().token);
          this.authenticationService.isLoginSubject.next(true);
        },
        (error) => {
          // this.alertService.error(error);
          this.loading = false;
        },
        () => {
          console.log('SUCCESS LOGIN!');
        });

  }

  logout() {
    this.authenticationService.logout();
    console.log('Successfull logout');
  }

}
