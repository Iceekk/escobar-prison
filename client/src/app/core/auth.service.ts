import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs';


@Injectable()
export class AuthService {

    isLoginSubject = new BehaviorSubject<boolean>(this.hasToken());

    constructor(private http: Http) { }


    private hasToken(): boolean {
        return !!localStorage.getItem('token');
    }

    public getToken(): string {
        return localStorage.getItem('token');
    }

    isLoggedIn(): Observable<boolean> {
        return this.isLoginSubject.asObservable();
    }

    login(username: string, password: string) {
        return this.http
            .post(`http://localhost:3000/login`, { username, password });
    }

    logout() {
        localStorage.removeItem('token');
        this.isLoginSubject.next(false);
    }

}
