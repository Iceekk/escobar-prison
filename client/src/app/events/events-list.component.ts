import { map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Event } from '../models/event.model';
import { AuthService } from '../core/auth.service';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.css']
})
export class EventsListComponent implements OnInit {

  events: Event[];

  constructor(
    private http: Http,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.getAllEvents().subscribe((data: any) => {
      this.events = data.json();
    });
  }

  getAllEvents() {
    return this.http.get(`http://localhost:3000/events`);
  }

  formatDate(date: string) {
    return date.slice(0, 10);
  }

}
