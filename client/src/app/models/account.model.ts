import { User } from './user.model';

export class Account {

    balance: number;

    moneyType: string;

    user: User;
}
