import { User } from './user.model';

export class Cell {

    cellNumber: number;

    category: string;

    isFull: boolean;

    capacity: number;

    users: User[];
}
