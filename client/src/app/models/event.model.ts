import { User } from './user.model';

export class Event {

    eventName: string;

    eventDesc: string;

    eventImage: string;

    eventDate: Date;

    users: User[];

}
