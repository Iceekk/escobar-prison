import { User } from './user.model';

export class Item {
    itemName: string;

    itemDesc: string;

    users: User[];

}
