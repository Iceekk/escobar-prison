import { Cell } from './cell.model';
import { Event } from './event.model';
import { Item } from './item.model';
import { Account } from './account.model';

export class User {

    firstName: string;

    lastName: string;

    username: string;

    rating: number;

    password: string;

    avatarUrl: string;

    role: string;

    events: Event[];

    cell: Cell;

    items: Item[];

    accounts: Account[];

}
