import { UserLoginDTO } from '../models/user/user-login.dto';
import { FileService } from '../common/core/file.service';
import { UserRegisterDTO } from '../models/user/user-register.dto';
import { UsersService } from '../common/core/users.service';
import { AuthService } from './auth.service';
import { Controller, Post, Body, FileInterceptor, UseInterceptors, UploadedFile, BadRequestException, UseGuards } from '@nestjs/common';
import { join } from 'path';
import { unlink } from 'fs';
import { Roles } from '../common';
import { AuthGuard } from '@nestjs/passport';

@Controller()
export class AuthController {

  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) { }

  @Post('login')
  async sign(@Body() user: UserLoginDTO): Promise<object> {

    const token = await this.authService.signIn(user);
    if (!token) {
      throw new BadRequestException('Wrong credentials!');
    }


    return { token };
  }

  @Post('add-prisoner')
  @Roles('ADMIN')
  @UseGuards(AuthGuard())
  @UseInterceptors(FileInterceptor('avatarUrl', {
    limits: FileService.fileLimit(1, 2 * 1024 * 1024),
    storage: FileService.storage(['public', 'images']),
    fileFilter: (req, file, cb) => FileService.fileFilter(req, file, cb, '.png', '.jpg'),
  }))
  async register(@Body() user: UserRegisterDTO, @UploadedFile() file): Promise<object> {

    const folder = join('.', 'public', 'uploads');
    if (file) {
      user.avatarUrl = join(folder, file.filename);
    } else {
      user.avatarUrl = join(folder, 'default.png');
    }
    await this.usersService.registerUser(user).catch(async (error) => {
      await new Promise((resolve, reject) => {
        if (file) {
          unlink(join('.', file.path), (err) => {
            if (err) {
              reject(error.message);
            }
            resolve();
          });
        }
        resolve();

        throw new BadRequestException(error.message);
      });
    });
    return { info: `New prisoner was added to Escobar's prison!` };
  }


}

