import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersService } from './users.service';
import { Module } from '@nestjs/common';
import { User } from './../../data/entities/users.entity';
import { FileService } from './file.service';
import { Cell } from './../../data/entities/cells.entity';
import { Item } from './../../data/entities/items.entity';
import { Event } from './../../data/entities/events.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Cell, Item, Event])],
  providers: [UsersService, FileService],
  exports: [UsersService, FileService, TypeOrmModule],
})
export class CoreModule { }
