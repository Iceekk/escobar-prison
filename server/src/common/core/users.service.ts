import { Item } from './../../data/entities/items.entity';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from './../../data/entities/users.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { UserLoginDTO } from './../../models/user/user-login.dto';
import { GetUserDTO } from './../../models/user/get-user.dto';
import * as bcrypt from 'bcrypt';
import { JwtPayload } from './../../interfaces/jwt-payload';
import { UserRegisterDTO } from 'src/models/user/user-register.dto';
import { Cell } from './../../data/entities/cells.entity';
import { Event } from '../../data/entities/events.entity';
import { CreateEventDTO } from '../..//models/user/create-event.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,

    @InjectRepository(Cell)
    private readonly cellsRepository: Repository<Cell>,

    @InjectRepository(Item)
    private readonly itemsRepository: Repository<Item>,

    @InjectRepository(Event)
    private readonly eventsRepository: Repository<Event>,

  ) { }

  async findUser(user): Promise<User> {
    const thisUser = await this.usersRepository.findOne({ where: { username: user.username } });

    if (!thisUser) {
      throw new HttpException('There is no such user', HttpStatus.NOT_FOUND);
    }
    return thisUser;
  }

  async addCell(cell): Promise<Cell[]> {
    const cellFound = await this.cellsRepository.findOne({ where: { cellNumber: cell.cellNumber } });

    if (cellFound) {
      throw new HttpException(`A cell with number ${cell.cellNumber} already exists!`, HttpStatus.NOT_ACCEPTABLE);
    }

    const newCell = this.cellsRepository.create(cell);
    return this.cellsRepository.save(newCell);
  }

  async getUserAccount(user) {
    const thisUser = await this.findUser(user);

    if (!thisUser) {
      throw new HttpException('There is no such user', HttpStatus.NOT_FOUND);
    }

    const userCell = await this.usersRepository.findOne({ username: user.username }, { relations: ['cell'] });

    const userItems: Item[] = await this.itemsRepository
      .createQueryBuilder('item')
      .leftJoinAndSelect('item.users', 'user')
      .where('user.username = :username', { username: thisUser.username })
      .select(['item.itemName', 'item.itemDesc'])
      .getMany();

    const userEvents: Event[] = await this.eventsRepository
      .createQueryBuilder('event')
      .leftJoinAndSelect('event.users', 'user')
      .where('user.username = :username', { username: thisUser.username })
      .select(['event.eventName', 'event.eventDesc', 'event.eventImage', 'event.eventDate'])
      .getMany();

    const userWithAccounts = await this.usersRepository.findOne({ username: user.username }, { relations: ['accounts'] });
    const userAccounts = [];
    userWithAccounts.accounts.forEach((account, index) => {

      const balance = Object.entries(account).slice(1)[index][0];
      const moneyType = Object.entries(account).slice(1)[index][1];
      const obj = {
        balance: balance,
        moneyType: moneyType
      };
      userAccounts.push(obj);
    });

    const generatedAccount = {
      avatar: thisUser.avatarUrl,
      firstName: thisUser.firstName,
      lastName: thisUser.lastName,
      username: thisUser.username,
      role: thisUser.role,
      raiting: thisUser.rating,
      cellNumber: userCell.cell.cellNumber,
      items: userItems,
      events: userEvents,
      accounts: userAccounts,
    };

    return generatedAccount;
  }

  async registerUser(user: UserRegisterDTO): Promise<User> {

    const userFound = await this.usersRepository.findOne({ where: { username: user.username } });

    if (userFound) {
      throw new HttpException(`User already exists!`, HttpStatus.CONFLICT);
    }
    const cell = await this.cellsRepository.findOne({ cellNumber: user.cellNumber }, { relations: ['users'] });

    if (cell.isFull) {
      throw new HttpException(`Cell Number ${cell.cellNumber} is at full capacity!`, HttpStatus.NOT_ACCEPTABLE);
    }

    user.password = await bcrypt.hash(user.password, 10);
    const newUser = this.usersRepository.create(user);
    const addedUser = await this.usersRepository.save(newUser);

    cell.users.push(newUser);
    if (cell.users.length == cell.capacity) {
      cell.isFull = true;
    }
    await this.cellsRepository.save(cell);


    return addedUser;
  }

  async validateUser(payload: JwtPayload): Promise<GetUserDTO> {

    const userFound: any = await this.usersRepository.findOne({ where: { username: payload.username } });
    return userFound;
  }

  async signIn(user: UserLoginDTO): Promise<GetUserDTO> {
    const userFound: GetUserDTO = await this.usersRepository
      .findOne({ select: ['firstName', 'lastName', 'username', 'password', 'role'], where: { username: user.username } });

    if (!userFound) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);
    }

    const result = await bcrypt.compare(user.password, userFound.password);
    if (!result) {
      throw new HttpException('Invalid credentails', HttpStatus.BAD_REQUEST);
    }

    return userFound;
  }

  async createEvent(event: CreateEventDTO): Promise<Event> {

    const eventFound = await this.eventsRepository.findOne({ where: { eventName: event.eventName } });

    if (eventFound) {
      throw new HttpException('Event already exists!', HttpStatus.CONFLICT);
    }

    event.eventDate = new Date();
    const newEvent = this.eventsRepository.create(event);
    const addedEvent = await this.eventsRepository.save(newEvent);

    return addedEvent;
  }

  async editEvent(event: CreateEventDTO): Promise<string> {
    const eventFound = await this.eventsRepository.findOne({ eventName: event.eventName });

    if (!eventFound) {
      throw new HttpException('Event not found!', HttpStatus.NOT_FOUND);
    }

    await this.eventsRepository.update(eventFound.event_id, {
      eventName: event.eventName, eventDesc: event.eventDesc, eventImage: event.eventImage, eventDate: event.eventDate,
    });

    return `Event ${event.eventName} was edited successfully!`;
  }

  async editPrisoner(user: UserRegisterDTO): Promise<string> {
    const userFound = await this.usersRepository.findOne({ username: user.username });

    if (!userFound) {
      throw new HttpException('User not found!', HttpStatus.NOT_FOUND);
    }

    await this.usersRepository.update(userFound.user_id, {
      firstName: user.firstName, lastName: user.lastName, rating: user.rating, role: user.role, avatarUrl: user.avatarUrl,
    });

    return `Prisoner ${user.username} was edited successfully!`;
  }

  async getEventParticipants(eventName: string): Promise<User[]> {
    const event = await this.eventsRepository.findOne({ eventName: `${eventName}` }, { relations: ['users'] });

    if (event.users.length === 0) {
      throw new HttpException(`Event ${eventName} does not have participants yet!`, HttpStatus.NOT_FOUND);
    }

    return event.users;
  }

  async joinEvent(user: User, eventName: string): Promise<string> {

    const eventFound = await this.eventsRepository.findOne({ eventName: `${eventName}` }, { relations: ['users'] });
    eventFound.users.push(user);
    await this.eventsRepository.save(eventFound);

    return `You successfully joined the ${eventName} event!`;
  }

  async leaveEvent(user: User, eventName: string): Promise<string> {
    const userFound = await this.usersRepository.findOne({ username: user.username }, { relations: ['events'] });
    const event = await this.eventsRepository.findOne({ eventName: `${eventName}` }, { relations: ['users'] });

    let index = -1;
    event.users.forEach((userr, idx) => {
      if (userr.username === user.username) {
        index = idx;
      }
    });

    if (index === -1) {
      throw new HttpException(`Participant not found!`, HttpStatus.NOT_FOUND);
    }

    event.users.splice(index, 1);
    userFound.events = [];
    await this.usersRepository.save(userFound);
    return `You successfully left event ${eventName}`;
  }

  async deleteEvent(eventName: string): Promise<string> {
    const foundEvent = await this.eventsRepository.findOne({ eventName: `${eventName}` });

    if (!foundEvent) {
      throw new HttpException(`Event not found!`, HttpStatus.NOT_FOUND);
    }

    await this.eventsRepository.remove(foundEvent);

    return `Event ${eventName} was deleted successfully!`;
  }

  async getEvents(): Promise<Event[]> {
    return await this.eventsRepository.find({ select: ['eventName', 'eventDesc', 'eventImage', 'eventDate'] });
  }

  async adminGetPrisoners(): Promise<User[]> {
    const usersFound = await this.usersRepository
      .find({ select: ['firstName', 'lastName', 'username', 'rating', 'avatarUrl', 'role'], relations: ['cell', 'items'] });
    return usersFound;
  }

  async userGetPrisoners(): Promise<User[]> {
    const usersFound = await this.usersRepository
      .find({ select: ['firstName', 'lastName', 'rating', 'avatarUrl'] });
    return usersFound;
  }

  async getPrisonerByUsername(username: string): Promise<User> {
    const userFound = await this.usersRepository
      .findOne({ select: ['firstName', 'lastName', 'username', 'rating', 'avatarUrl', 'role'], where: { username: `${username}` } });

    if (!userFound) {
      throw new HttpException(`Prisoner not found!`, HttpStatus.NOT_FOUND);
    }
    return userFound;
  }

  async getCells(): Promise<Cell[]> {
    return this.cellsRepository.find({ select: ['cellNumber', 'category', 'isFull', 'capacity'] });
  }

  async getAverageRaitingOfPrison(): Promise<number> {
    const prisoners = await this.usersRepository.find({ select: ['rating'] });

    let sumRaiting = 0;
    prisoners.forEach((user) => {
      sumRaiting += user.rating;
    });
    const avgRaiting = sumRaiting / prisoners.length;

    return avgRaiting;
  }

  async deletePrisoner(username: string): Promise<string> {
    const userFound = await this.usersRepository.findOne({ username: `${username}` });
    if (!userFound) {
      throw new HttpException(`Prisoner not found!`, HttpStatus.NOT_FOUND);
    }
    await this.usersRepository.remove(userFound);
    return `Prisoner ${username} was successfully removed!`;
  }

}
