import { Column, PrimaryGeneratedColumn, Entity, ManyToOne } from 'typeorm';
import { IsNumber, IsPositive } from 'class-validator';
import { User } from './users.entity';

@Entity({
    name: 'accounts',
})
export class Account {

    @PrimaryGeneratedColumn()
    account_id: number;

    @Column()
    @IsNumber()
    @IsPositive()
    balance: number;

    @Column({ enum: ['BGN', 'USD', 'GBN'], type: 'enum' })
    moneyType: string;

    @ManyToOne(type => User, user => user.accounts)
    userr: User;

}
