import { Column, PrimaryGeneratedColumn, Entity, OneToMany } from 'typeorm';
import { IsNumber, IsPositive, IsBoolean, Max, IsArray } from 'class-validator';
import { User } from './users.entity';

@Entity({
    name: 'cells',
})
export class Cell {

    @PrimaryGeneratedColumn()
    cell_id: number;

    @Column()
    @IsNumber()
    @IsPositive()
    cellNumber: number;

    @Column({ enum: ['VIP', 'NORMAL', 'ISOLATOR'], type: 'enum' })
    category: string;

    @Column({ default: false })
    @IsBoolean()
    isFull: boolean;

    @Column()
    @IsNumber()
    @Max(2)
    capacity: number;

    @IsArray()
    @OneToMany(type => User, user => user.cell, { cascade: true })
    users: User[];

}
