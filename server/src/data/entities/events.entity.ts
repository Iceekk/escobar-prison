import { Column, PrimaryGeneratedColumn, Entity, ManyToMany, JoinTable } from 'typeorm';
import { IsString, IsDate, IsArray } from 'class-validator';
import { User } from './users.entity';

@Entity({
    name: 'events',
})
export class Event {

    @PrimaryGeneratedColumn()
    event_id: number;

    @Column()
    @IsString()
    eventName: string;

    @Column()
    @IsString()
    eventDesc: string;

    @Column({ nullable: true })
    eventImage: string;

    @Column()
    @IsDate()
    eventDate: Date;

    @IsArray()
    @ManyToMany(type => User, user => user.events, { cascade: true })
    @JoinTable()
    users: User[];

}
