import { Column, PrimaryGeneratedColumn, Entity, ManyToMany} from 'typeorm';
import { IsString, IsArray } from 'class-validator';
import { User } from './users.entity';

@Entity({
    name: 'items',
})
export class Item {

    @PrimaryGeneratedColumn()
    item_id: number;

    @Column()
    @IsString()
    itemName: string;

    @Column()
    @IsString()
    itemDesc: string;

    @IsArray()
    @ManyToMany(type => User, user => user.items)
    users: User[];

}
