import { Account } from './accounts.entity';
import { Cell } from './cells.entity';
import { Column, PrimaryGeneratedColumn, Entity, ManyToMany, JoinTable, ManyToOne, OneToMany } from 'typeorm';
import { IsString, MinLength, IsNumber, IsPositive, IsArray, IsInstance } from 'class-validator';
import { Event } from './events.entity';
import { Item } from './items.entity';

@Entity({
    name: 'users',
})
export class User {

    @PrimaryGeneratedColumn()
    user_id: number;

    @Column()
    @IsString()
    firstName: string;

    @Column()
    @IsString()
    lastName: string;

    @Column()
    @IsString()
    username: string;

    @Column()
    @IsNumber()
    @IsPositive()
    rating: number;

    @Column()
    @MinLength(5)
    password: string;

    @Column({ nullable: true })
    avatarUrl: string;

    @Column({ enum: ['USER', 'ADMIN'], type: 'enum' })
    role: string;

    @IsArray()
    @ManyToMany(type => Event, event => event.users)
    events: Event[];

    @IsInstance(Cell)
    @ManyToOne(type => Cell, cell => cell.users)
    cell: Cell;

    @IsArray()
    @ManyToMany(type => Item, item => item.users, { cascade: true })
    @JoinTable()
    items: Item[];

    @IsArray()
    @OneToMany(type => Account, account => account.userr, { cascade: true })
    accounts: Account[];

}
