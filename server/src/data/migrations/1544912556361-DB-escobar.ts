import {MigrationInterface, QueryRunner} from "typeorm";

export class DBEscobar1544912556361 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `cells` (`cell_id` int NOT NULL AUTO_INCREMENT, `cellNumber` int NOT NULL, `category` enum ('VIP', 'NORMAL', 'ISOLATOR') NOT NULL, `isFull` tinyint NOT NULL DEFAULT 0, `capacity` int NOT NULL, PRIMARY KEY (`cell_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `events` (`event_id` int NOT NULL AUTO_INCREMENT, `eventName` varchar(255) NOT NULL, `eventDesc` varchar(255) NOT NULL, `eventImage` varchar(255) NULL, `eventDate` datetime NOT NULL, PRIMARY KEY (`event_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `items` (`item_id` int NOT NULL AUTO_INCREMENT, `itemName` varchar(255) NOT NULL, `itemDesc` varchar(255) NOT NULL, PRIMARY KEY (`item_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`user_id` int NOT NULL AUTO_INCREMENT, `firstName` varchar(255) NOT NULL, `lastName` varchar(255) NOT NULL, `username` varchar(255) NOT NULL, `rating` int NOT NULL, `password` varchar(255) NOT NULL, `avatarUrl` varchar(255) NULL, `role` enum ('USER', 'ADMIN') NOT NULL, `cellCellId` int NULL, PRIMARY KEY (`user_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `accounts` (`account_id` int NOT NULL AUTO_INCREMENT, `balance` int NOT NULL, `moneyType` enum ('BGN', 'USD', 'GBN') NOT NULL, `userrUserId` int NULL, PRIMARY KEY (`account_id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users_events_events` (`usersUserId` int NOT NULL, `eventsEventId` int NOT NULL, PRIMARY KEY (`usersUserId`, `eventsEventId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users_items_items` (`usersUserId` int NOT NULL, `itemsItemId` int NOT NULL, PRIMARY KEY (`usersUserId`, `itemsItemId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_58346cccf0634ccd8c40f5596b7` FOREIGN KEY (`cellCellId`) REFERENCES `cells`(`cell_id`)");
        await queryRunner.query("ALTER TABLE `accounts` ADD CONSTRAINT `FK_5e52464e1f542a4f957889ff642` FOREIGN KEY (`userrUserId`) REFERENCES `users`(`user_id`)");
        await queryRunner.query("ALTER TABLE `users_events_events` ADD CONSTRAINT `FK_2817fd08baac7700b2d51ff8175` FOREIGN KEY (`usersUserId`) REFERENCES `users`(`user_id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `users_events_events` ADD CONSTRAINT `FK_aa42ade36bc713d308b6b229fb5` FOREIGN KEY (`eventsEventId`) REFERENCES `events`(`event_id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `users_items_items` ADD CONSTRAINT `FK_2cc48a3c338e51c87a08350f1eb` FOREIGN KEY (`usersUserId`) REFERENCES `users`(`user_id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `users_items_items` ADD CONSTRAINT `FK_92b2e37f587f808d6f3974b5c0d` FOREIGN KEY (`itemsItemId`) REFERENCES `items`(`item_id`) ON DELETE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `users_items_items` DROP FOREIGN KEY `FK_92b2e37f587f808d6f3974b5c0d`");
        await queryRunner.query("ALTER TABLE `users_items_items` DROP FOREIGN KEY `FK_2cc48a3c338e51c87a08350f1eb`");
        await queryRunner.query("ALTER TABLE `users_events_events` DROP FOREIGN KEY `FK_aa42ade36bc713d308b6b229fb5`");
        await queryRunner.query("ALTER TABLE `users_events_events` DROP FOREIGN KEY `FK_2817fd08baac7700b2d51ff8175`");
        await queryRunner.query("ALTER TABLE `accounts` DROP FOREIGN KEY `FK_5e52464e1f542a4f957889ff642`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_58346cccf0634ccd8c40f5596b7`");
        await queryRunner.query("DROP TABLE `users_items_items`");
        await queryRunner.query("DROP TABLE `users_events_events`");
        await queryRunner.query("DROP TABLE `accounts`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP TABLE `items`");
        await queryRunner.query("DROP TABLE `events`");
        await queryRunner.query("DROP TABLE `cells`");
    }

}
