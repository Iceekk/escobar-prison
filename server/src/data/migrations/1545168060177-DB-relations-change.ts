import {MigrationInterface, QueryRunner} from "typeorm";

export class DBRelationsChange1545168060177 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `events_users_users` (`eventsEventId` int NOT NULL, `usersUserId` int NOT NULL, PRIMARY KEY (`eventsEventId`, `usersUserId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `events` CHANGE `eventDate` `eventDate` datetime NOT NULL");
        await queryRunner.query("ALTER TABLE `events_users_users` ADD CONSTRAINT `FK_254db2de89a07aa5ff006c2d9ee` FOREIGN KEY (`eventsEventId`) REFERENCES `events`(`event_id`) ON DELETE CASCADE");
        await queryRunner.query("ALTER TABLE `events_users_users` ADD CONSTRAINT `FK_580e16bb49d545ba5455f45abf3` FOREIGN KEY (`usersUserId`) REFERENCES `users`(`user_id`) ON DELETE CASCADE");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `events_users_users` DROP FOREIGN KEY `FK_580e16bb49d545ba5455f45abf3`");
        await queryRunner.query("ALTER TABLE `events_users_users` DROP FOREIGN KEY `FK_254db2de89a07aa5ff006c2d9ee`");
        await queryRunner.query("ALTER TABLE `events` CHANGE `eventDate` `eventDate` datetime(0) NOT NULL");
        await queryRunner.query("DROP TABLE `events_users_users`");
    }

}
