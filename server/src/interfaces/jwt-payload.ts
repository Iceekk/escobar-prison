export interface JwtPayload {
  username: string;

  firstName: string;

  role: string;
}
