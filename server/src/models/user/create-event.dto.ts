import { IsString, IsNumber } from 'class-validator';
import { Optional } from '@nestjs/common';

export class CreateEventDTO {

    @IsString()
    eventName: string;

    @IsString()
    eventDesc: string;

    @IsNumber()
    participantCapacity: number;

    @Optional()
    eventImage: string;

    @IsString()
    eventDate: Date;

}
