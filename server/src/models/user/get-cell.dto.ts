import { User } from 'src/data/entities/users.entity';

export class GetCellDTO {

    cellNumber: number;

    category: string;

    isFull: boolean;

    capacity: string;

    users: User[];
}
