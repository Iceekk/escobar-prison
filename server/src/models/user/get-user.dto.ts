export class GetUserDTO {

  username: string;

  firstName: string;

  lastName: string;

  password: string;

  role: string;

}
