import { IsString, IsNumber, IsArray } from 'class-validator';
import { Optional } from '@nestjs/common';
import { Item } from 'src/data/entities/items.entity';
import { Account } from 'src/data/entities/accounts.entity';
import { Event } from 'src/data/entities/events.entity';

export class UserRegisterDTO {

  @IsString()
  firstName: string;

  @IsString()
  lastName: string;

  @IsString()
  username: string;

  @IsNumber()
  rating: number;

  @IsString()
  password: string;

  @Optional()
  avatarUrl: string;

  @IsString()
  role: string;

  @IsNumber()
  cellNumber: number;

  @Optional()
  @IsArray()
  items: Item[];

  @Optional()
  @IsArray()
  accounts: Account[];

  @IsArray()
  events: Event[];

}
