import { UserRegisterDTO } from '../../models/user/user-register.dto';
import { JwtServiceMock } from '../mocks/jwt.service.mock';
import { UserLoginDTO } from '../../models/user/user-login.dto';
import { AuthController } from '../../auth/auth.controller';
import { UsersService } from '../../common/core/users.service';
import { Test } from '@nestjs/testing';
import { AuthService } from '../../auth/auth.service';
import { PassportModule } from '@nestjs/passport';

jest.mock('../../auth/auth.service');
jest.mock('../../common/core/users.service');

  describe('AuthController', () => {
    let authService: AuthService = new AuthService(null, null);
    let authController: AuthController;
    let jwtServiceMock: JwtServiceMock;

  beforeAll(async () => {
    jwtServiceMock = new JwtServiceMock({});
    const module = await Test.createTestingModule({
      imports: [PassportModule.register({
        defaultStrategy: 'jwt',
      })],
      controllers: [AuthController],
      providers: [UsersService,
        {
          provide: AuthService,
          useValue: authService,
        },
        {
          provide: 'UserRepository',
          useValue: {
            findOne: () => {
              return 'user';
            },
          },
        }],
    }).compile();

    authController = module.get<AuthController>(AuthController);
  });

  it('should call AuthService signIn method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const authServ = new AuthService(userService, null);
    const authCtrl = new AuthController(authServ, userService);
    const user = new UserLoginDTO();

    jest.spyOn(authServ, 'signIn').mockImplementation(() => {
      return 'token';
    });
    // Act
    await authCtrl.sign(user);
    // Assert
    expect(authServ.signIn).toHaveBeenCalledTimes(1);
  });
  it('should call UserService registerUser method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const authService = new AuthService(userService, null);
    const authController = new AuthController(authService, userService);
    const user = new UserRegisterDTO();

    jest.spyOn(userService, 'registerUser').mockImplementation(() => {
      return { catch: ()=> {} };
    });
    // Act
    await authController.register(user, '');

    // Assert
    expect(userService.registerUser).toHaveBeenCalledTimes(1);
  });

})