import { GetCellDTO } from './../../models/user/get-cell.dto';
import { UsersController } from './../../users/users.controller';
import { UserRegisterDTO } from '../../models/user/user-register.dto';
import { UserLoginDTO } from '../../models/user/user-login.dto';
import { UsersService } from '../../common/core/users.service';
import { Test } from '@nestjs/testing';
import { CreateEventDTO } from '../../models/user/create-event.dto';
import { registerDecorator } from 'class-validator';

jest.mock('../../common/core/users.service');

  describe('UsersController', () => {
    let userService: UsersService = new UsersService(null, null, null, null);
    let userController: UsersController;

  beforeAll(async () => {
    
    const module = await Test.createTestingModule({
      
      controllers: [UsersController],
      providers: [UsersService,
        {
          provide: 'UserRepository',
          useValue: {
            findOne: () => {
              return 'user';
            },
          },
        }],
    }).compile();

    userController = module.get<UsersController>(UsersController);
  });

  it('should call (GET) getEvents method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    

    jest.spyOn(userService, 'getEvents').mockImplementation(() => {
      return {};
    });
    // Act
    await userCtrl.getEvents();
    // Assert
    expect(userService.getEvents).toHaveBeenCalledTimes(1);
  });

  it('should call (GET) adminGetPrisoners method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    const user = new UserRegisterDTO();
    user.role = 'ADMIN';
    const request = {
      user
    }

    jest.spyOn(userService, 'adminGetPrisoners').mockImplementation(() => {
      return [];
    });
    // Act
    await userCtrl.prisoners(request);
    // Assert
    expect(userService.adminGetPrisoners).toHaveBeenCalledTimes(1);
  });

  it('should call (GET) userGetPrisoners method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    const user = new UserRegisterDTO();
    user.role = 'USER';
    const request = {
      user
    }

    jest.spyOn(userService, 'userGetPrisoners').mockImplementation(() => {
      return [];
    });
    // Act
    await userCtrl.prisoners(request);
    // Assert
    expect(userService.userGetPrisoners).toHaveBeenCalledTimes(1);
  });

  it('should call (POST) add-cell method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    const cell = new GetCellDTO();

    jest.spyOn(userService, 'getUserAccount').mockImplementation(() => {
      return {};
    });
    // Act
    await userCtrl.addCell(cell);
    // Assert
    expect(userService.addCell).toHaveBeenCalledTimes(1);
  });

  it('should call (GET) my-account method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    const user = new UserLoginDTO();

    jest.spyOn(userService, 'getUserAccount').mockImplementation(() => {
      return {};
    });
    // Act
    await userCtrl.myAccount(user);
    // Assert
    expect(userService.getUserAccount).toHaveBeenCalledTimes(1);
  });

  it('should call (GET) cells method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);

    jest.spyOn(userService, 'getCells').mockImplementation(() => {
      return [];
    });
    // Act
    await userCtrl.cells();
    // Assert
    expect(userService.getCells).toHaveBeenCalledTimes(1);
  });

  it('should call (POST) edit-event method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    const event = new CreateEventDTO();
    jest.spyOn(userService, 'editEvent').mockImplementation(() => {
      return { catch: ()=> {} };
    });
    
    // Act
    await userCtrl.editEvent(event, null);
    // Assert
    expect(userService.editEvent).toHaveBeenCalledTimes(1);
  });



  it('should call (POST) delete-event method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    const event = new CreateEventDTO();
    jest.spyOn(userService, 'deleteEvent').mockImplementation(() => {
      return '';
    });
    
    // Act
    await userCtrl.deleteEvent('PrisonEvent');
    // Assert
    expect(userService.deleteEvent).toHaveBeenCalledTimes(1);
  });


  it('should call (GET) get-event-participants', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    
    jest.spyOn(userService, 'getEventParticipants').mockImplementation(() => {
      return [];
    });
    
    // Act
    await userCtrl.getEventParticipants('PrisonEvent');
    // Assert
    expect(userService.getEventParticipants).toHaveBeenCalledTimes(1);
  });

  it('should call (POST) leave-event', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    const user = new UserLoginDTO();
    const request = {
      user
    }
    jest.spyOn(userService, 'leaveEvent').mockImplementation(() => {
      return '';
    });
    
    // Act
    await userCtrl.leaveEvent(user, 'PrisonEvent');
    // Assert
    expect(userService.leaveEvent).toHaveBeenCalledTimes(1);
  });


  it('should call (GET) avg-rait-of-prison', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    
    jest.spyOn(userService, 'getAverageRaitingOfPrison').mockImplementation(() => {
      return '';
    });
    
    // Act
    await userCtrl.getAvgRaitOfPrison();
    // Assert
    expect(userService.getAverageRaitingOfPrison).toHaveBeenCalledTimes(1);
  });

  it('should call UsersService (POST) prisonerByUsername method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);

    jest.spyOn(userService, 'getPrisonerByUsername').mockImplementation(() => {
      return {};
    });
    // Act
    await userCtrl.getPrisonerByUsername('peter');
    // Assert
    expect(userService.getPrisonerByUsername).toHaveBeenCalledTimes(1);
  });

  it('should call (POST) delete-prisoner method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    jest.spyOn(userService, 'deletePrisoner').mockImplementation(() => {
      return '';
    });
    
    // Act
    await userCtrl.deletePrisoner('username');
    // Assert
    expect(userService.deletePrisoner).toHaveBeenCalledTimes(1);
  });

  
  it('should call (POST) edit-prisoner method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    const user = new UserRegisterDTO();
    
    jest.spyOn(userService, 'editPrisoner').mockImplementation(() => {
      return { catch: ()=> { return {} } };
    });
    
    // Act
    await userCtrl.editPrisoner(user, '');
    // Assert
    expect(userService.editPrisoner).toHaveBeenCalledTimes(1);
  });


  it('should call (POST) add-event method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    const event = new CreateEventDTO();
    
    jest.spyOn(userService, 'createEvent').mockImplementation(() => {
      return { catch: ()=> { return {} } };
    });
    
    // Act
    await userCtrl.createEvent(event, '');
    // Assert
    expect(userService.createEvent).toHaveBeenCalledTimes(1);
  });

  it('should call (POST) join-event method', async () => {
    // Arrange
    const userService = new UsersService(null, null, null, null);
    const userCtrl = new UsersController(userService);
    const user = new UserRegisterDTO();
    
    jest.spyOn(userService, 'joinEvent').mockImplementation(() => {
      return '';
    });
    
    // Act
    await userCtrl.joinEvent(user, 'PrisonEvent');
    // Assert
    expect(userService.joinEvent).toHaveBeenCalledTimes(1);
  });













});