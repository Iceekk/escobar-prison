import { JwtServiceMock } from "./jwt.service.mock";

describe('JwtMockService', () => {

    it('method sign to return correct result', () => {

        // Arrange
        const jwtMockService = new JwtServiceMock({});

        // Act
        const result = jwtMockService.sign('');

        // Assert
        expect(result).toBe('token');
    });

    it('method verify to return correct result', () => {

        // Arrange
        const jwtMockService = new JwtServiceMock({});

        // Act
        const result = jwtMockService.verify('');

        // Assert
        expect(result).toBe('verified');
    });

    it('method decode to return correct result', () => {

        // Arrange
        const jwtMockService = new JwtServiceMock({});

        // Act
        const result = jwtMockService.decode('', '');

        // Assert
        expect(result).toBe('decoded');
    });

});