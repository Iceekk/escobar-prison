import { JwtService } from '@nestjs/jwt';
import { UserLoginDTO } from './../../models/user/user-login.dto';
import { AuthService } from './../../auth/auth.service';
import { TestingModule, Test } from "@nestjs/testing";
import { UsersService } from "../../common/core/users.service";
import { getRepositoryToken } from "@nestjs/typeorm";
import { User } from "../../data/entities/users.entity";
import { Cell } from "../../data/entities/cells.entity";
import { Item } from "../../data/entities/items.entity";
import { Event } from "../../data/entities/events.entity";
import { PassportModule } from "@nestjs/passport";
import { JwtServiceMock } from '../mocks/jwt.service.mock';
import { JwtPayload } from '../../interfaces/jwt-payload';

describe('AuthService', () => {

    let userRepository: any = {
        save: () => { return {}; }
    };
    let cellRepository: any = {};
    let itemRepository: any = {};
    let eventRepository: any = {};

    let jwtServiceMock: JwtServiceMock;
    let userService: any = {
        signIn: () => { return {} }
    };
    let authService: AuthService;

    beforeAll(async () => {

        jwtServiceMock = new JwtServiceMock({});

        const module: TestingModule = await Test.createTestingModule({
            imports: [PassportModule.register({
                defaultStrategy: 'jwt',
            })],
            providers: [AuthService]
        }).overrideProvider(getRepositoryToken(User))
            .useValue(userRepository)
            .overrideProvider(getRepositoryToken(Cell))
            .useValue(cellRepository)
            .overrideProvider(getRepositoryToken(Item))
            .useValue(itemRepository)
            .overrideProvider(getRepositoryToken(Event))
            .useValue(eventRepository)
            .overrideProvider(UsersService)
            .useValue(userService)
            .overrideProvider(JwtService)
            .useValue(jwtServiceMock).compile();

        authService = module.get<AuthService>(AuthService);

    });

    describe('method', () => {
        it('signIn should be defined', () => {

            // Arrange
            userRepository.save = () => { return {} };
            const user: UserLoginDTO = new UserLoginDTO();

            const result = jest.spyOn(authService, 'signIn');
            authService.signIn(user);

            // Аct & Assert
            expect(result).toBeDefined();

        });

        it('signIn should be called', () => {

            // Arrange
            userRepository.save = () => { return {} };
            const user: UserLoginDTO = new UserLoginDTO();

            const result = jest.spyOn(authService, 'signIn');
            authService.signIn(user);

            // Аct & Assert
            expect(result).toHaveBeenCalled();

        });

        it('signIn should return null', async () => {

            // Arrange
            userService.signIn = () => { return null; }
            userRepository.save = () => { return null };
            const user: UserLoginDTO = new UserLoginDTO();

            jest.spyOn(authService, 'signIn');

            // Act
            const result = await authService.signIn(user);

            // Assert
            expect(result).toBe(null);

        });

        it('validateUser should be defined', () => {

            // Arrange
            const payload: JwtPayload = { username: '', firstName: '', role: '' };
            const result = jest.spyOn(authService, 'validateUser');
            authService.validateUser(payload);

            // Аct & Assert
            expect(result).toBeDefined();

        });

        it('validateUser should be called', () => {

            // Arrange
            const payload: JwtPayload = { username: '', firstName: '', role: '' };
            const result = jest.spyOn(authService, 'validateUser');
            authService.validateUser(payload);

            // Аct & Assert
            expect(result).toHaveBeenCalled();

        });
    });

});