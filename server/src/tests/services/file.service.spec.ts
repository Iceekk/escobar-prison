import { FileService } from './../../common/core/file.service';

describe('FileService', () => {

    it('method fileLimit to have been called 1 time', () => {

        // Arrange
        const result = jest.spyOn(FileService, 'fileLimit');

        // Act
        FileService.fileLimit(5, 5);

        // Assert
        expect(result).toHaveBeenCalledTimes(1);
    });

    it('method fileLimit to return correct result be defined', () => {

        // Arrange & Act & Assert
        expect(FileService.fileLimit(5, 5)).toBeDefined();
    });

    it('method fileLimit to return correct result', () => {

        // Arrange
        jest.spyOn(FileService, 'fileLimit').mockImplementation(() => {
            return 'obj';
        });

        // Act
        const result = FileService.fileLimit(5, 5);

        // Assert
        expect(result).toBe('obj');
    });

    it('method storage to have been called 1 time', () => {

        // Arrange
        const result = jest.spyOn(FileService, 'storage');

        // Act
        FileService.storage([]);

        // Assert
        expect(result).toHaveBeenCalledTimes(1);
    });

    it('method storage to return correct result be defined', () => {

        // Arrange & Act & Assert
        expect(FileService.storage([])).toBeDefined();
    });

    it('method storage to return correct result', () => {

        // Arrange
        jest.spyOn(FileService, 'storage').mockImplementation(() => {
            return 'obj';
        });

        // Act
        const result = FileService.storage([]);

        // Assert
        expect(result).toBe('obj');
    });

});