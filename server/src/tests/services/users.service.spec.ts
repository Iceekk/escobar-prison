import { CreateEventDTO } from './../../models/user/create-event.dto';
import { JwtPayload } from './../../interfaces/jwt-payload';
import { UserRegisterDTO } from './../../models/user/user-register.dto';
import { UsersService } from '../../common/core/users.service';
import { Test, TestingModule } from '@nestjs/testing';
import { User } from '../../data/entities/users.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Cell } from '../../data/entities/cells.entity';
import { Item } from '../../data/entities/items.entity';
import { Event } from '../../data/entities/events.entity';

describe('UsersService', () => {

    let userRepository: any = {
        findOne: () => { return null },
        find: () => { return null },
        update: () => { return {} },
        remove: () => { return {} }
    };
    let itemRepository: any = {
        createQueryBuilder: () => {
            leftJoinAndSelect: () => { }
        },
    };
    let cellRepository: any = {
        findOne: () => { return { capacity: 0, users: {} } },
        find: () => { return null },
        create: () => { return [] },
        save: () => { return [] }
    };
    let eventRepository: any = {
        findOne: () => { return null },
        find: () => { return [] },
        create: () => { return [] },
        save: () => { return [] },
        update: () => { return {} },
        remove: () => { return {} }
    };
    let userService: UsersService;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [UsersService],

        }).overrideProvider(getRepositoryToken(User))
            .useValue(userRepository)
            .overrideProvider(getRepositoryToken(Cell))
            .useValue(cellRepository)
            .overrideProvider(getRepositoryToken(Item))
            .useValue(itemRepository)
            .overrideProvider(getRepositoryToken(Event))
            .useValue(eventRepository).compile();

        userService = module.get<UsersService>(UsersService);

    });

    describe('calling', () => {


        it('registerUser method should be called', async () => {
            userRepository.findOne = () => {
                return null;
            }
            userRepository.create = () => {
                return {};
            }
            userRepository.save = () => {
                return {};
            }
            const user: UserRegisterDTO = new UserRegisterDTO();
            user.password = '12345';

            const result = jest.spyOn(userService, 'registerUser');

            userService.registerUser(user);

            expect(result).toHaveBeenCalledTimes(1);
        })


        it('findUser method should be called', async () => {
            userRepository.findOne = () => {
                return {};
            }
            const user: User = new User();

            const result = jest.spyOn(userService, 'findUser');

            await userService.findUser(user);

            expect(result).toHaveBeenCalledTimes(1);
        })


        it('addCell method should be called', async () => {
            cellRepository.findOne = () => {
                return null;
            }
            cellRepository.create = () => {
                return [];
            }
            cellRepository.save = () => {
                return [];
            }
            const cell: Cell = new Cell();

            const result = jest.spyOn(userService, 'addCell');

            userService.addCell(cell);

            expect(result).toHaveBeenCalledTimes(1);
        })


        it('getUserAccount method should be called', async () => {
            userRepository.findOne = () => {
                return {};
            }
            const user: User = new User();

            const result = jest.spyOn(userService, 'getUserAccount');

            userService.getUserAccount(user);

            expect(result).toHaveBeenCalledTimes(1);
        })

        it('validateUser method should be called', async () => {
            userRepository.findOne = () => {
                return {};
            }
            const user: User = new User();

            const result = jest.spyOn(userService, 'validateUser');

            userService.validateUser(user);

            expect(result).toHaveBeenCalledTimes(1);
        })

        it('createEvent method should be called', async () => {
            eventRepository.findOne = () => {
                return '';
            }
            eventRepository.create = () => {
                return '';
            }
            eventRepository.save = () => {
                return '';
            }

            const event: CreateEventDTO = new CreateEventDTO();

            const result = jest.spyOn(userService, 'createEvent');

            userService.createEvent(event);

            expect(result).toHaveBeenCalledTimes(1);
        })

        it('editEvent method should be called', async () => {
            eventRepository.findOne = () => {
                return {};
            }
            const event: CreateEventDTO = new CreateEventDTO();

            const result = jest.spyOn(userService, 'editEvent');

            userService.editEvent(event);

            expect(result).toHaveBeenCalledTimes(1);
        })

        it('getEventParticipants method should be called', async () => {
            eventRepository.findOne = () => {
                return { users: {} };
            }
            const eventName: string = '';

            const result = jest.spyOn(userService, 'getEventParticipants');

            userService.getEventParticipants(eventName);

            expect(result).toHaveBeenCalledTimes(1);
        })

        it('joinEvent method should be called', async () => {

            eventRepository.findOne = () => { return { users: { push: () => { return {} } } } };

            const eventName: string = '';
            const user: User = new User();


            const result = jest.spyOn(userService, 'joinEvent');

            userService.joinEvent(user, eventName);

            expect(result).toHaveBeenCalledTimes(1);
        })

        it('deleteEvent method should be called', async () => {
            eventRepository.findOne = () => {
                return {};
            }

            const eventName: string = '';

            const result = jest.spyOn(userService, 'deleteEvent');

            userService.deleteEvent(eventName);

            expect(result).toHaveBeenCalledTimes(1);
        })

        it('getEvents method should be called', async () => {
            eventRepository.find = () => {
                return [];
            }
            const result = jest.spyOn(userService, 'getEvents');

            userService.getEvents();

            expect(result).toHaveBeenCalledTimes(1);
        })

        it('editPrisoner method should be called', async () => {
            userRepository.findOne = () => {
                return {};
            }
            userRepository.update = () => {
                return {};
            }
            const user: UserRegisterDTO = new UserRegisterDTO();

            const result = jest.spyOn(userService, 'editPrisoner');

            userService.editPrisoner(user);

            expect(result).toHaveBeenCalledTimes(1);
        })

        it('deletePrisoner method should be called', async () => {
            userRepository.findOne = () => {
                return {};
            }
            userRepository.remove = () => {
                return null;
            }
            const username: string = '';

            const result = jest.spyOn(userService, 'deletePrisoner');

            userService.deletePrisoner(username);

            expect(result).toHaveBeenCalledTimes(1);
        })

        it('adminGetPrisoners method should be called', async () => {
            userRepository.find = () => {
                return { users: {} };
            }
            const result = jest.spyOn(userService, 'adminGetPrisoners');

            userService.adminGetPrisoners();

            expect(result).toHaveBeenCalledTimes(1);
        })

        it('userGetPrisoners method should be called', async () => {
            userRepository.find = () => {
                return {};
            }
            const result = jest.spyOn(userService, 'userGetPrisoners');

            userService.userGetPrisoners();

            expect(result).toHaveBeenCalledTimes(1);
        })


        it('getPrisonerByUsername method should be called', async () => {
            userRepository.findOne = () => {
                return {};
            }
            const user: User = new User();
            user.username = 'Pesho'
            const result = jest.spyOn(userService, 'getPrisonerByUsername');

            userService.getPrisonerByUsername(user.username);

            expect(result).toHaveBeenCalledTimes(1);
        })

        it('getCells method should be called', async () => {
            cellRepository.find = () => {
                return {};
            }
            const username: string = '';
            const result = jest.spyOn(userService, 'getCells');

            userService.getCells();

            expect(result).toHaveBeenCalledTimes(1);
        })

        it('getAverageRaitingOfPrison method should be called', async () => {
            userRepository.find = () => {
                return [];
            }
            userRepository.findOne = () => {
                return { users: { username: 'Pesho' } };
            }

            const result = jest.spyOn(userService, 'getAverageRaitingOfPrison');

            userService.getAverageRaitingOfPrison();

            expect(result).toHaveBeenCalledTimes(1);
        })

    });

    describe('george', () => {


        it('findUser should find User', async () => {
            const user: User = new User();
            user.username = 'Peter';
            const returnedUser = {
                user_id: 1,
                username: 'Peter'
            }

            userRepository.findOne = () => {
                return returnedUser;
            }

            const data = await userService.findUser(user);

            expect(data).toEqual(returnedUser);
        });

        it('should throw if user not found', async () => {
            const user: User = new User();
            user.username = '';
            let error = '';
            userRepository.findOne = () => {
                return null;
            }
            try {
                await userService.findUser(user)
            }
            catch (err) {
                error = err.response;
            }

            expect(error).toBe('There is no such user');
            // () => { expect(() => userService.findUser(user)).toThrow(); };
        });

        it('registerUserregister should throw if user found', async () => {
            const cell: Cell = new Cell;
            cell.cellNumber = 111;

            const user: UserRegisterDTO = new UserRegisterDTO();
            user.firstName = 'Peter';
            user.lastName = 'Koev';
            user.username = 'pkoev';
            user.rating = 5;
            user.password = 'pkoev';
            user.role = 'USER';
            user.cellNumber = cell.cellNumber;
            user.items = [{ itemName: 'Peter\'s phone', itemDesc: '', item_id: 99, users: [] }];

            userRepository.findOne = () => {
                return user;
            }
            userRepository.create = () => {
                return user;
            }
            userRepository.save = () => {
                return user;
            }
            cellRepository.findOne = () => { return { users: { push: () => { return {} } } } };

            let error = '';
            try {
                await userService.registerUser(user);
            }
            catch (err) {
                error = err.response;
            }

            expect(error).toBe('User already exists!');
        });

    });

    describe('method', () => {
        let userRepository: any = {
            findOne: () => { return null },
            find: () => { return null },
            update: () => { return {} },
            create: () => { return {} },
            remove: () => { return {} }
        };
        let itemRepository: any = {};
        let cellRepository: any = {
            findOne: () => { return { capacity: 0, users: {} } },
            find: () => { return null },
            create: () => { return [] },
            save: () => { return [] }
        };
        let eventRepository: any = {
            findOne: () => { return null },
            find: () => { return [] },
            create: () => { return [] },
            save: () => { return [] },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let userService: UsersService;

        beforeAll(async () => {

            const module: TestingModule = await Test.createTestingModule({
                providers: [UsersService],

            }).overrideProvider(getRepositoryToken(User))
                .useValue(userRepository)
                .overrideProvider(getRepositoryToken(Cell))
                .useValue(cellRepository)
                .overrideProvider(getRepositoryToken(Item))
                .useValue(itemRepository)
                .overrideProvider(getRepositoryToken(Event))
                .useValue(eventRepository).compile();

            userService = module.get<UsersService>(UsersService);

        });

        it('registerUser should be defined', () => {

            // Arrange
            const user: UserRegisterDTO = new UserRegisterDTO();
            user.username = 'Petkan';
            user.password = '12345';

            // Act & Assert
            expect(userService.registerUser(user)).toBeDefined();
        });

        it('findUser should be defined', async () => {

            // Arrange
            userRepository.findOne = () => {
                return {};
            }
            const user: User = new User();

            // Act & Assert
            expect(userService.findUser(user)).toBeDefined();
        });

        it('addCell should be defined', async () => {

            // Arrange
            cellRepository.findOne = () => {
                return null;
            }
            const cell: Cell = new Cell();

            // Act & Assert
            expect(userService.addCell(cell)).toBeDefined();
        });

        it('validateUser should be defined', async () => {

            // Arrange
            userRepository.findOne = () => { return {} };
            const payload: JwtPayload = { username: '', firstName: '', role: '' };

            // Act & Assert
            expect(userService.validateUser(payload)).toBeDefined();
        });

        it('createEvent should be defined', async () => {

            // Arrange
            const event: CreateEventDTO = new CreateEventDTO();

            // Act & Assert
            expect(userService.createEvent(event)).toBeDefined();
        });

        it('editEvent should be defined', async () => {

            // Arrange
            eventRepository.findOne = () => { return {} }
            const event: CreateEventDTO = new CreateEventDTO();

            // Act & Assert
            expect(userService.editEvent(event)).toBeDefined();
        });

        it('editPrisoner should be defined', async () => {

            // Arrange
            userRepository.findOne = () => { return {} };
            const user: UserRegisterDTO = new UserRegisterDTO();

            // Act & Assert
            expect(userService.editPrisoner(user)).toBeDefined();
        });

        it('getEventParticipants should be defined', async () => {

            // Arrange
            eventRepository.findOne = () => { return { users: 2 } };

            // Act & Assert
            expect(userService.getEventParticipants('eventName')).toBeDefined();
        });

        it('joinEvent should be defined', async () => {

            // Arrange
            const user: User = new User();
            eventRepository.findOne = () => { return { users: { push: () => { return {} } } } };

            // Act & Assert
            expect(userService.joinEvent(user, 'eventName')).toBeDefined();
        });

        it('deleteEvent should be defined', async () => {

            // Arrange
            eventRepository.findOne = () => { return {} };

            // Act & Assert
            expect(userService.deleteEvent('eventName')).toBeDefined();
        });

        it('getEvents should be defined', async () => {

            // Arrange & Act & Assert
            expect(userService.getEvents()).toBeDefined();
        });

        it('adminGetPrisoners should be defined', async () => {

            // Arrange
            userRepository.find = () => { return [] };

            // Act & Assert
            expect(userService.adminGetPrisoners()).toBeDefined();
        });

        it('userGetPrisoners should be defined', async () => {

            // Arrange
            userRepository.find = () => { return [] };

            // Act & Assert
            expect(userService.userGetPrisoners()).toBeDefined();
        });

        it('getPrisonerByUsername should be defined', async () => {

            // Arrange
            userRepository.findOne = () => { return {} };

            // Act & Assert
            expect(userService.getPrisonerByUsername('username')).toBeDefined();
        });

        it('getCells should be defined', async () => {

            // Arrange
            cellRepository.find = () => { return [] };

            // Act & Assert
            expect(userService.getCells()).toBeDefined();
        });

        it('getAverageRaitingOfPrison should be defined', async () => {

            // Arrange
            userRepository.find = () => { return [] };

            // Act & Assert
            expect(userService.getAverageRaitingOfPrison()).toBeDefined();
        });

        it('deletePrisoner should be defined', async () => {

            // Arrange
            userRepository.findOne = () => { return {} };

            // Act & Assert
            expect(userService.deletePrisoner('username')).toBeDefined();
        });

    });

    describe('deletePrisoner', () => {

        let userRepository: any = {
            findOne: () => { return null },
            find: () => { return null },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let itemRepository: any = {
            createQueryBuilder: () => {
            },
        };
        let cellRepository: any = {
            findOne: () => { return { capacity: 0, users: {} } },
            find: () => { return null },
            create: () => { return [] },
            save: () => { return [] }
        };
        let eventRepository: any = {
            findOne: () => { return null },
            find: () => { return [] },
            create: () => { return [] },
            save: () => { return [] },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let userService: UsersService;

        beforeAll(async () => {
            const module: TestingModule = await Test.createTestingModule({
                providers: [UsersService],

            }).overrideProvider(getRepositoryToken(User))
                .useValue(userRepository)
                .overrideProvider(getRepositoryToken(Cell))
                .useValue(cellRepository)
                .overrideProvider(getRepositoryToken(Item))
                .useValue(itemRepository)
                .overrideProvider(getRepositoryToken(Event))
                .useValue(eventRepository).compile();

            userService = module.get<UsersService>(UsersService);

        });

        it('should return correct data', async () => {

            // Arrange
            userRepository.findOne = () => {
                return {};
            }
            jest.spyOn(userService, 'deletePrisoner');

            // Act
            const result = await userService.deletePrisoner('Petkan');

            // Assert
            expect(result).toBe(`Prisoner Petkan was successfully removed!`);

        });

        it('should throw error', async () => {
            // Arrange
            userRepository.findOne = () => {
                return null;
            }
            let error = '';
            // Act & Assert

            try {
                await userService.deletePrisoner('Petkan');

            } catch (err) {
                error = err.response;
            }
            expect(error).toBe('Prisoner not found!');

        });

        it('userRepositoty, findOne method should be called', async () => {

            // Arrange
            userRepository.findOne = () => {
                return {};
            }
            const result = jest.spyOn(userRepository, 'findOne');

            // Act 
            userService.deletePrisoner('Petkan');

            // Assert
            expect(result).toHaveBeenCalledTimes(1);

        });

    });

    describe('getPrisonerByUsername', () => {

        let userRepository: any = {
            findOne: () => { return null },
            find: () => { return null },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let itemRepository: any = {
            createQueryBuilder: () => {
            },
        };
        let cellRepository: any = {
            findOne: () => { return { capacity: 0, users: {} } },
            find: () => { return null },
            create: () => { return [] },
            save: () => { return [] }
        };
        let eventRepository: any = {
            findOne: () => { return null },
            find: () => { return [] },
            create: () => { return [] },
            save: () => { return [] },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let userService: UsersService;

        beforeAll(async () => {
            const module: TestingModule = await Test.createTestingModule({
                providers: [UsersService],

            }).overrideProvider(getRepositoryToken(User))
                .useValue(userRepository)
                .overrideProvider(getRepositoryToken(Cell))
                .useValue(cellRepository)
                .overrideProvider(getRepositoryToken(Item))
                .useValue(itemRepository)
                .overrideProvider(getRepositoryToken(Event))
                .useValue(eventRepository).compile();

            userService = module.get<UsersService>(UsersService);

        });

        it('should return correct data', async () => {

            // Arrange
            userRepository.findOne = () => {
                return 'John';
            }
            jest.spyOn(userService, 'getPrisonerByUsername');

            // Act
            const result = await userService.getPrisonerByUsername('John');

            // Assert
            expect(result).toBe('John');

        });

        it('should throw error', async () => {

            // Arrange
            userRepository.findOne = () => {
                return null;
            }
            let error = '';
            // Act & Assert
            try {
                await userService.getPrisonerByUsername('John')
            } catch (err) {
                error = err.response;
            }
            expect(error).toBe('Prisoner not found!');
        });

        it('userRepositoty, findOne method should be called', async () => {

            // Arrange
            userRepository.findOne = () => {
                return {};
            }
            const result = jest.spyOn(userRepository, 'findOne');

            // Act 
            userService.getPrisonerByUsername('John');

            // Assert
            expect(result).toHaveBeenCalledTimes(1);

        });

    });

    describe('deleteEvent', () => {

        let userRepository: any = {
            findOne: () => { return null },
            find: () => { return null },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let itemRepository: any = {
            createQueryBuilder: () => {
            },
        };
        let cellRepository: any = {
            findOne: () => { return { capacity: 0, users: {} } },
            find: () => { return null },
            create: () => { return [] },
            save: () => { return [] }
        };
        let eventRepository: any = {
            findOne: () => { return null },
            find: () => { return [] },
            create: () => { return [] },
            save: () => { return [] },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let userService: UsersService;

        beforeAll(async () => {
            const module: TestingModule = await Test.createTestingModule({
                providers: [UsersService],

            }).overrideProvider(getRepositoryToken(User))
                .useValue(userRepository)
                .overrideProvider(getRepositoryToken(Cell))
                .useValue(cellRepository)
                .overrideProvider(getRepositoryToken(Item))
                .useValue(itemRepository)
                .overrideProvider(getRepositoryToken(Event))
                .useValue(eventRepository).compile();

            userService = module.get<UsersService>(UsersService);

        });

        it('should return correct data', async () => {
            // Arrange
            eventRepository.findOne = () => {
                return 'fight';
            }
            jest.spyOn(userService, 'deleteEvent');

            // Act
            const result = await userService.deleteEvent('fight');

            // Assert
            expect(result).toBe(`Event fight was deleted successfully!`);

        });

        it('should throw error', async () => {

            // Arrange
            eventRepository.findOne = () => {
                return null;
            }
            let error = '';
            // Act & Assert
            try {
                await userService.deleteEvent('fight')
            } catch (err) {
                error = err.response;
            }
            expect(error).toBe('Event not found!');
        });

        it('eventRepositoty, findOne method should be called', async () => {

            // Arrange
            eventRepository.findOne = () => {
                return {};
            }
            const result = jest.spyOn(eventRepository, 'findOne');

            // Act 
            userService.deleteEvent('fight');

            // Assert
            expect(result).toHaveBeenCalledTimes(1);

        });

    });

    describe('joinEvent', () => {

        let userRepository: any = {
            findOne: () => { return null },
            find: () => { return null },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let itemRepository: any = {
            createQueryBuilder: () => {
            },
        };
        let cellRepository: any = {
            findOne: () => { return { capacity: 0, users: {} } },
            find: () => { return null },
            create: () => { return [] },
            save: () => { return [] }
        };
        let eventRepository: any = {
            findOne: () => { return null },
            find: () => { return [] },
            create: () => { return [] },
            save: () => { return [] },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let userService: UsersService;

        beforeAll(async () => {
            const module: TestingModule = await Test.createTestingModule({
                providers: [UsersService],

            }).overrideProvider(getRepositoryToken(User))
                .useValue(userRepository)
                .overrideProvider(getRepositoryToken(Cell))
                .useValue(cellRepository)
                .overrideProvider(getRepositoryToken(Item))
                .useValue(itemRepository)
                .overrideProvider(getRepositoryToken(Event))
                .useValue(eventRepository).compile();

            userService = module.get<UsersService>(UsersService);

        });

        it('should return correct data', async () => {

            // Arrange
            eventRepository.findOne = () => {
                return { users: { push: () => { return {} } } };
            }
            jest.spyOn(userService, 'deleteEvent');
            const user: User = new User();

            // Act
            const result = await userService.joinEvent(user, 'fight');

            // Assert
            expect(result).toBe(`You successfully joined the fight event!`);

        });

        it('eventRepositoty, findOne method should be called', async () => {

            // Arrange
            eventRepository.findOne = () => {
                return {};
            }
            const result = jest.spyOn(eventRepository, 'findOne');
            const user: User = new User();

            // Act 
            userService.joinEvent(user, 'fight');

            // Assert
            expect(result).toHaveBeenCalledTimes(1);

        });

    });

    describe('editEvent', () => {

        let userRepository: any = {
            findOne: () => { return null },
            find: () => { return null },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let itemRepository: any = {
            createQueryBuilder: () => {
            },
        };
        let cellRepository: any = {
            findOne: () => { return { capacity: 0, users: {} } },
            find: () => { return null },
            create: () => { return [] },
            save: () => { return [] }
        };
        let eventRepository: any = {
            findOne: () => { return null },
            find: () => { return [] },
            create: () => { return [] },
            save: () => { return [] },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let userService: UsersService;

        beforeAll(async () => {
            const module: TestingModule = await Test.createTestingModule({
                providers: [UsersService],

            }).overrideProvider(getRepositoryToken(User))
                .useValue(userRepository)
                .overrideProvider(getRepositoryToken(Cell))
                .useValue(cellRepository)
                .overrideProvider(getRepositoryToken(Item))
                .useValue(itemRepository)
                .overrideProvider(getRepositoryToken(Event))
                .useValue(eventRepository).compile();

            userService = module.get<UsersService>(UsersService);

        });

        it('should return correct data', async () => {

            // Arrange
            eventRepository.findOne = () => {
                return { users: { push: () => { return {} } } };
            }
            jest.spyOn(userService, 'editEvent');
            const event: CreateEventDTO = new CreateEventDTO();
            event.eventName = 'fight';

            // Act
            const result = await userService.editEvent(event);

            // Assert
            expect(result).toBe(`Event fight was edited successfully!`);

        });

        it('should throw error', async () => {

            // Arrange
            eventRepository.findOne = () => {
                return null;
            }
            const event: CreateEventDTO = new CreateEventDTO();
            event.eventName = 'fight';
            let error = '';

            // Act & Assert
            try {
                await userService.editEvent(event)
            } catch (err) {
                error = err.response
            }
            expect(error).toBe('Event not found!');

        });

        it('eventRepositoty, findOne method should be called', async () => {

            // Arrange
            eventRepository.findOne = () => {
                return {};
            }
            const result = jest.spyOn(eventRepository, 'findOne');
            const event: CreateEventDTO = new CreateEventDTO();
            event.eventName = 'fight';
            // Act 
            userService.editEvent(event);

            // Assert
            expect(result).toHaveBeenCalledTimes(1);

        });

    });

    describe('createEvent', () => {

        let userRepository: any = {
            findOne: () => { return null },
            find: () => { return null },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let itemRepository: any = {
            createQueryBuilder: () => {
            },
        };
        let cellRepository: any = {
            findOne: () => { return { capacity: 0, users: {} } },
            find: () => { return null },
            create: () => { return [] },
            save: () => { return [] }
        };
        let eventRepository: any = {
            findOne: () => { return null },
            find: () => { return [] },
            create: () => { return {} },
            save: () => { return {} },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let userService: UsersService;

        beforeAll(async () => {
            const module: TestingModule = await Test.createTestingModule({
                providers: [UsersService],

            }).overrideProvider(getRepositoryToken(User))
                .useValue(userRepository)
                .overrideProvider(getRepositoryToken(Cell))
                .useValue(cellRepository)
                .overrideProvider(getRepositoryToken(Item))
                .useValue(itemRepository)
                .overrideProvider(getRepositoryToken(Event))
                .useValue(eventRepository).compile();

            userService = module.get<UsersService>(UsersService);

        });

        it('should return correct data', async () => {

            // Arrange
            eventRepository.findOne = () => {
                return null;
            }
            eventRepository.save = () => {
                return 'saved';
            }
            jest.spyOn(userService, 'createEvent');
            const event: CreateEventDTO = new CreateEventDTO();
            event.eventName = 'fight';

            // Act
            const result = await userService.createEvent(event);

            // Assert
            expect(result).toBe('saved');

        });

        it('should throw error', async () => {

            // Arrange
            const event: CreateEventDTO = new CreateEventDTO();
            event.eventName = 'fight';
            eventRepository.findOne = () => {
                return {};
            }

            let error = '';
            // Act & Assert
            try {
                await userService.createEvent(event)
            } catch (err) {
                error = err.response
            }
            expect(error).toBe('Event already exists!');

        });

        it('eventRepositoty, findOne method should be called', async () => {

            // Arrange
            eventRepository.findOne = () => {
                return {};
            }

            const result = jest.spyOn(eventRepository, 'findOne');
            const event: CreateEventDTO = new CreateEventDTO();
            event.eventName = 'fight';
            // Act 
            userService.createEvent(event);

            // Assert
            expect(result).toHaveBeenCalledTimes(1);

        });

    });

    describe('editPrisoner', () => {

        let userRepository: any = {
            findOne: () => { return null },
            find: () => { return null },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let itemRepository: any = {
            createQueryBuilder: () => {
            },
        };
        let cellRepository: any = {
            findOne: () => { return { capacity: 0, users: {} } },
            find: () => { return null },
            create: () => { return [] },
            save: () => { return [] }
        };
        let eventRepository: any = {
            findOne: () => { return null },
            find: () => { return [] },
            create: () => { return [] },
            save: () => { return [] },
            update: () => { return {} },
            remove: () => { return {} }
        };
        let userService: UsersService;

        beforeAll(async () => {
            const module: TestingModule = await Test.createTestingModule({
                providers: [UsersService],

            }).overrideProvider(getRepositoryToken(User))
                .useValue(userRepository)
                .overrideProvider(getRepositoryToken(Cell))
                .useValue(cellRepository)
                .overrideProvider(getRepositoryToken(Item))
                .useValue(itemRepository)
                .overrideProvider(getRepositoryToken(Event))
                .useValue(eventRepository).compile();

            userService = module.get<UsersService>(UsersService);

        });

        it('should return correct data', async () => {

            // Arrange
            userRepository.findOne = () => {
                return 'John';
            }
            const user: UserRegisterDTO = new UserRegisterDTO();
            user.username = 'John';
            jest.spyOn(userService, 'editPrisoner');

            // Act
            const result = await userService.editPrisoner(user);

            // Assert
            expect(result).toBe(`Prisoner John was edited successfully!`);

        });

        it('should throw error', async () => {

            // Arrange
            userRepository.findOne = () => {
                return null;
            }
            const user: UserRegisterDTO = new UserRegisterDTO();
            user.username = 'John';

            let error = '';
            // Act & Assert
            try {
                await userService.editPrisoner(user)
            } catch (err) {
                error = err.response
            }

        });

        it('userRepositoty, findOne method should be called', async () => {
            // Arrange
            userRepository.findOne = () => {
                return {};
            }
            const result = jest.spyOn(userRepository, 'findOne');
            const user: UserRegisterDTO = new UserRegisterDTO();
            user.username = 'John';

            // Act 
            userService.editPrisoner(user);

            // Assert
            expect(result).toHaveBeenCalledTimes(1);

        });

    });

});
