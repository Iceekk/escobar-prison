import { UserRegisterDTO } from './../models/user/user-register.dto';
import { AuthGuard } from '@nestjs/passport';
import { Controller, Get, UseGuards, Post, Body, Req, UploadedFile, UseInterceptors, FileInterceptor, BadRequestException } from '@nestjs/common';
import { UsersService } from './../common/core/users.service';
import { Roles } from '../common';
import { User } from '../data/entities/users.entity';
import { Event } from '../data/entities/events.entity';
import { CreateEventDTO } from '../models/user/create-event.dto';
import { join } from 'path';
import { unlink } from 'fs';
import { FileService } from '../common/core/file.service';
import { Cell } from '../data/entities/cells.entity';

@Controller()
export class UsersController {

  constructor(private readonly usersService: UsersService) { }

  @Get('events')
  // @Roles('USER', 'ADMIN')
  //  @UseGuards(AuthGuard())
  async getEvents(): Promise<Event[]> {
    return await this.usersService.getEvents();
  }

  @Get('prisoners')
  @Roles('USER', 'ADMIN')
  @UseGuards(AuthGuard())
  async prisoners(@Req() request): Promise<User[]> {

    const user = request.user;
    if (user.role === 'ADMIN') {

      return await this.usersService.adminGetPrisoners();

    } else if (user.role === 'USER') {

      return await this.usersService.userGetPrisoners();
    }
  }

  @Post('add-cell')
  @Roles('ADMIN')
  @UseGuards(AuthGuard())
  async addCell(@Body() cell): Promise<string> {

    await this.usersService.addCell(cell);
    return 'Cell was successfully added!';
  }

  @Get('my-account')
  @Roles('USER', 'ADMIN')
  @UseGuards(AuthGuard())
  async myAccount(@Req() request): Promise<string> {

    const user = request.user;
    const userAccount: any = await this.usersService.getUserAccount(user);

    return userAccount;
  }

  @Get('cells')
  @Roles('ADMIN')
  @UseGuards(AuthGuard())
  cells(): Promise<Cell[]> {
    return this.usersService.getCells();
  }

  @Post('edit-event')
  @Roles('ADMIN')
  @UseGuards(AuthGuard())
  async editEvent(@Body() event: CreateEventDTO, @UploadedFile() file): Promise<object> {

    const folder = join('.', 'public', 'uploads');
    if (!file) {
      event.eventImage = join(folder, 'default.png');
    } else {
      event.eventImage = join(folder, file.filename);
    }

    await this.usersService.editEvent(event).catch(async (error) => {
      await new Promise((resolve, reject) => {
        if (file) {
          unlink(join('.', file.path), (err) => {
            if (err) {
              reject(error.message);
            }
            resolve();
          });
        }
        resolve();
        throw new BadRequestException(error.message);
      });
    });
    return { info: 'Event was edited successfully!' };
  }

  @Post('delete-event')
  @Roles('ADMIN')
  @UseGuards(AuthGuard())
  async deleteEvent(@Body() obj): Promise<string> {

    const eventName = obj.eventName;
    const deletedEvent = await this.usersService.deleteEvent(eventName);
    return deletedEvent;
  }

  @Get('get-event-participants')
  @Roles('ADMIN', 'USER')
  @UseGuards(AuthGuard())
  getEventParticipants(@Body() eventObj): Promise<User[]> {
    return this.usersService.getEventParticipants(eventObj.eventName);
  }

  @Post('leave-event')
  @Roles('USER')
  @UseGuards(AuthGuard())
  leaveEvent(@Req() request, @Body() eventObj): Promise<string> {

    const user = request.user;
    return this.usersService.leaveEvent(user, eventObj.eventName);
  }

  @Get('avg-rait-of-prison')
  getAvgRaitOfPrison(): Promise<number> {
    return this.usersService.getAverageRaitingOfPrison();
  }

  @Post('get-prisoner-by-username')
  @Roles('ADMIN')
  @UseGuards(AuthGuard())
  getPrisonerByUsername(@Body() obj): Promise<User> {
    const username = obj.username;
    return this.usersService.getPrisonerByUsername(username);
  }

  @Post('delete-prisoner')
  @Roles('ADMIN')
  @UseGuards(AuthGuard())
  deletePrisoner(@Body() obj): Promise<string> {

    const username = obj.username;
    return this.usersService.deletePrisoner(username);
  }

  @Post('edit-prisoner')
  @Roles('ADMIN')
  @UseGuards(AuthGuard())
  async editPrisoner(@Body() user: UserRegisterDTO, @UploadedFile() file): Promise<object> {

    const folder = join('.', 'public', 'uploads');
    if (!file) {
      user.avatarUrl = join(folder, 'default.png');
    } else {
      user.avatarUrl = join(folder, file.filename);
    }

    await this.usersService.editPrisoner(user).catch(async (error) => {
      await new Promise((resolve, reject) => {
        if (file) {
          unlink(join('.', file.path), (err) => {
            if (err) {
              reject(error.message);
            }
            resolve();
          });
        }
        resolve();

        throw new BadRequestException(error.message);
      });
    });
    return { info: 'Event was edited!' };
  }


  @Post('add-event')
  @Roles('ADMIN')
  @UseGuards(AuthGuard())
  @UseInterceptors(FileInterceptor('eventUrl', {
    limits: FileService.fileLimit(1, 2 * 1024 * 1024),
    storage: FileService.storage(['public', 'images']),
    fileFilter: (req, file, cb) => FileService.fileFilter(req, file, cb, '.png', '.jpg'),
  }))
  async createEvent(@Body() event: CreateEventDTO, @UploadedFile() file): Promise<object> {

    const folder = join('.', 'public', 'uploads');
    if (!file) {
      event.eventImage = join(folder, 'default.png');
    } else {
      event.eventImage = join(folder, file.filename);
    }

    await this.usersService.createEvent(event).catch(async (error) => {
      await new Promise((resolve, reject) => {
        if (file) {
          unlink(join('.', file.path), (err) => {
            if (err) {
              reject(error.message);
            }
            resolve();
          });
        }
        resolve();

        throw new BadRequestException(error.message);
      });
    });
    return { info: `Event ${event.eventName} was created successfully!` };
  }

  @Post('join-event')
  @Roles('USER')
  @UseGuards(AuthGuard())
  joinEvent(@Req() request, @Body() eventObj): Promise<string> {

    const user: User = request.user;
    const joinedUser = this.usersService.joinEvent(user, eventObj.eventName);

    return joinedUser;
  }

}
